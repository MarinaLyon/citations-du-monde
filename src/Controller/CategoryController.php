<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Quote;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryController extends Controller
{
    /**
     * @Route("/category/allCategories", name="all_categories")
     */
    public function findAll(CategoryRepository $repo)
    {
        $categories = $repo->findAll();

        return $this->render('category/allCategories.html.twig', [
            'controller_name' => 'CategoryController',
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/addCategoryAdmin", name="add_category_admin", methods="GET|POST")
     * @Route("/category/{id}/edit", name="edit_category")
     */
    public function formCategory(Category $category = null, Request $request, ObjectManager $manager):response {
        if (!$category) {
            $category = new Category();
        }

        $form = $this->CreateForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('one_category', ['id' => $category->getId()]);
        }

        return $this->render('category/addCategoryAdmin.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'editMode' => $category->getId() !== null
        ]);
    }

    /**
     * @Route("/category/showCategory/{id}", name="one_category")
     */
    public function show(Category $category)
    {
        return $this->render('category/oneCategory.html.twig', [
            'category' => $category,
        ]);
    }



    //Cette fonction permet de récupérer toutes les citations d'une seule catégorie
    /**
     * @Route("view/category/{category}", name="view_category_quotes")
     */
    public function index(Category $category, CategoryRepository $repo)
    {
        $repo = $this->getDoctrine()->getRepository(Quote::class);
        $quotes = $category->getQuotes();
        return $this->render('category/categoryQuotes.html.twig', [
            'category' => $category, 
            'quotes' => $quotes
        ]);
    }

    
    //Cette fonction permet de récuperer toutes les catégories dans la barre de nav
    // public function findAll()
    //     {
    //         $repo = $this->getDoctrine()->getRepository(Category::class);
    
    //         $category = $repo->findAll();
    
    //         return $this->render(
    //             'header.html.twig',
    //             array('category' => $category)
    //         );
    //     }
    
    
    
    /**
     * @Route("/category/deleteCategory/{id}", name="delete_category")
     */
    public function delete(Category $category)
    {   
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('all_categories');
    }
}