<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Quote;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {        
        $repo = $this->getDoctrine()->getRepository(Quote::class);
        $quotes = $repo->findAll(['id' => 'DESC']);
        return $this->render('home.html.twig', [
            'quotes' => $quotes,
        ]);
    }    
}